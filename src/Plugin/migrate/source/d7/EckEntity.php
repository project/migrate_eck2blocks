<?php

namespace Drupal\migrate_eck2blocks\Plugin\migrate\source\d7;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Drupal 7 ECK Entity source from database.
 *
 * @MigrateSource(
 *   id = "d7_eck_entity",
 *   source_module = "eck"
 * )
 */
class EckEntity extends FieldableEntity {

  /**
   * The Entity Type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The Bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('eck_' . $this->entityType, 'eck')->condition('type', $this->bundle)->fields('eck');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Get Field API field values.
    $id = $row->getSourceProperty('id');
    foreach (array_keys($this->getFields($this->entityType, $this->bundle)) as $field) {
      $row->setSourceProperty($field, $this->getFieldValues($this->entityType, $field, $id));
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The primary identifier for the entity'),
      'type' => $this->t('The bundle of the entity'),
      'title' => $this->t('Title'),
      'uid' => $this->t('Author'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
      'language' => $this->t('Entity language'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_manager) {
    $this->entityType = $configuration['entity_type'];
    $this->bundle = $configuration['bundle'];

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
  }

}
