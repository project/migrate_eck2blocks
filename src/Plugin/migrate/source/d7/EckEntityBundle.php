<?php

namespace Drupal\migrate_eck2blocks\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 ECK Entity bundle source from database.
 *
 * @MigrateSource(
 *   id = "d7_eck_entity_bundle",
 *   source_module = "eck"
 * )
 */
class EckEntityBundle extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('eck_bundle', 'eckb')->fields('eckb');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The primary identifier for a bundle'),
      'entity_type' => $this->t('The entity type this bundle belongs to'),
      'name' => $this->t("The bundle's name"),
      'label' => $this->t('A human readable name for the bundle (not that the type is not human readable)'),
      'config' => $this->t('A serialized list of the bundle settings.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
    ];
  }

}
