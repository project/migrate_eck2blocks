<?php

namespace Drupal\migrate_eck2blocks\Plugin\Derivative;

use Drupal\migrate_eck2blocks\Plugin\migrate\destination\EckInstance;
use Drupal\migrate\Plugin\Derivative\MigrateEntity;

/**
 * Deriver for ECK instance migration destination.
 *
 * @see \Drupal\migrate_eck2blocks\Plugin\migrate\destination\EckInstance
 */
class MigrateEckEntity extends MigrateEntity {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['instance'] = [
      'id' => "eck:instance",
      'class' => EckInstance::class,
      'requirements_met' => 1,
    ];
    return $this->derivatives;
  }

}
