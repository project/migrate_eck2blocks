<?php

/**
 * @file
 * Primary hook implementations for the Migrate ECK2Blocks module.
 */

use Drupal\Core\Database\Connection;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Implements hook_migrate_prepare_row().
 */
function migrate_eck2blocks_migrate_prepare_row(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
  // Change the D7 field and field instance configurations; because of the
  // similarities between these two data structures, handle them the same.
  if ($migration->id() == 'd7_field' || $migration->id() == 'upgrade_d7_field'
      || $migration->id() == 'd7_field_instance' || $migration->id() == 'upgrade_d7_field_instance') {
    // Keep a static list of fields which are processed, so if the field shows
    // up again then it needs to be skipped.
    static $processed = [];

    // Get a list of ECK entity types; if none are found, there's no point in
    // continuing further.
    $eck_types = migrate_eck2blocks_list_entity_types($source->getDatabase());
    if (empty($eck_types)) {
      return;
    }

    // Update the main "entity_type" value.
    if (in_array($row->getSourceProperty('entity_type'), $eck_types)) {
      // Update the primary entity_type property.
      $row->setSourceProperty('entity_type', 'block_content');

      // Also check the the similar attribute from Commerce Migrate, if present.
      $commerce1_entity_type = $row->getSourceProperty('commerce1_entity_type');
      if (!empty($commerce1_entity_type)) {
        $row->setSourceProperty('commerce1_entity_type', 'block_content');
      }
    }

    // Update the "entity_types" array.
    $entity_types = $row->getSourceProperty('entity_types');
    if (!empty($entity_types)) {
      $modified = FALSE;
      foreach ($entity_types as $key => $entity_type) {
        if (in_array($entity_type, $eck_types)) {
          $entity_types[$key] = 'block_content';
          $modified = TRUE;
        }
      }
      if ($modified) {
        $row->setSourceProperty('entity_types', $entity_types);
      }
    }

    // Update the "instances" array.
    $instances = $row->getSourceProperty('instances');
    if (!empty($instances)) {
      // Look to see if this field added to any ECK bundles.
      $eck_field = FALSE;
      foreach ($instances as $key => $instance) {
        if (in_array($instance['entity_type'], $eck_types)) {
          $eck_field = TRUE;
        }
      }
      if ($eck_field) {
        // Regenerate the list of instances to combine all instances into this
        // one field definition.
        $field_name = $row->getSourceProperty('field_name');
        if (!isset($processed[$field_name])) {
          $instances = $source->getDatabase()
            ->select('field_config_instance', 'fci')
            ->fields('fci')
            ->condition('field_name', $field_name)
            ->condition('entity_type', $eck_types, 'IN')
            ->execute()
            ->fetchAll(PDO::FETCH_ASSOC);
          foreach ($instances as $key => $instance) {
            $instances[$key]['entity_type'] = 'block_content';
          }
          $row->setSourceProperty('instances', $instances);
          // Don't process this field again.
          $processed[$field_name] = TRUE;
        }

        // This field was processed once already, so force Migrate API to skip
        // this row.
        else {
          return FALSE;
        }
      }
    }
  }
}

/**
 * Get a list of entity types from ECK.
 *
 * @param \Drupal\Core\Database\Connection $database
 *   A D7 database.
 *
 * @return array
 *   List of ECK entity types.
 */
function migrate_eck2blocks_list_entity_types(Connection $database) {
  static $eck_types;

  if (is_null($eck_types)) {
    try {
      $eck_types = $database->query("SELECT name FROM {eck_entity_type}")->fetchCol();
    }
    catch (Exception $e) {
      $eck_types = [];
    }
  }

  return $eck_types;
}

/**
 * Implements hook_migration_plugins_alter().
 */
function migrate_eck2blocks_migration_plugins_alter(array &$definitions) {
  foreach ($definitions as &$definition) {
    if (isset($definition['destination']['plugin'])) {
      if ($definition['destination']['plugin'] == 'd7_field') {
        $definition['migration_dependencies']['optional'][] = 'd7_eck_bundles';
      }
      if ($definition['destination']['plugin'] == 'upgrade_d7_field') {
        $definition['migration_dependencies']['optional'][] = 'upgrade_d7_eck_bundles';
      }
    }
  }
}
